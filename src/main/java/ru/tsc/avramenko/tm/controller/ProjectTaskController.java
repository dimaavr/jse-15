package ru.tsc.avramenko.tm.controller;

import ru.tsc.avramenko.tm.api.controller.IProjectTaskController;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.api.service.IProjectTaskService;
import ru.tsc.avramenko.tm.api.service.ITaskService;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectService projectService, ITaskService taskService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        Task taskToProject = projectTaskService.bindTaskById(projectId, taskId);
        if (taskToProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        Task taskFromProject = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskFromProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public void findAllTasksByProjectId() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        if (tasks == null) throw new TaskNotFoundException();
        int index = 0;
        for (Task task : tasks) {
            index++;
            System.out.println(index + ". " + task.toString());
        }
    }

    @Override
    public void removeProjectById() {
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Project removedProject = projectTaskService.removeProjectById(projectId);
        if (removedProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        final Project removedProject = projectTaskService.removeProjectByIndex(index);
        if (removedProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) throw new ProjectNotFoundException();
        final Project removedProject = projectTaskService.removeProjectByName(name);
        if (removedProject == null) throw new ProcessException();
        else System.out.println("[OK]");
    }

}