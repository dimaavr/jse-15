package ru.tsc.avramenko.tm.api.controller;

import ru.tsc.avramenko.tm.model.Project;

public interface IProjectController {

    void showList();

    void showFindProject(Project project);

    void clearProjects();

    void createProject();

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByName();

    void changeStatusByIndex();

}
