package ru.tsc.avramenko.tm.api.entity;

import ru.tsc.avramenko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
