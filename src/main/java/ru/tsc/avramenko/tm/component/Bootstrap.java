package ru.tsc.avramenko.tm.component;

import ru.tsc.avramenko.tm.api.controller.ICommandController;
import ru.tsc.avramenko.tm.api.controller.IProjectController;
import ru.tsc.avramenko.tm.api.controller.IProjectTaskController;
import ru.tsc.avramenko.tm.api.controller.ITaskController;
import ru.tsc.avramenko.tm.api.repository.ICommandRepository;
import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.api.service.ICommandService;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.api.service.IProjectTaskService;
import ru.tsc.avramenko.tm.api.service.ITaskService;
import ru.tsc.avramenko.tm.constant.ArgumentConst;
import ru.tsc.avramenko.tm.constant.TerminalConst;
import ru.tsc.avramenko.tm.controller.CommandController;
import ru.tsc.avramenko.tm.controller.ProjectController;
import ru.tsc.avramenko.tm.controller.ProjectTaskController;
import ru.tsc.avramenko.tm.controller.TaskController;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.system.UnknownCommandException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.repository.CommandRepository;
import ru.tsc.avramenko.tm.repository.ProjectRepository;
import ru.tsc.avramenko.tm.repository.TaskRepository;
import ru.tsc.avramenko.tm.service.CommandService;
import ru.tsc.avramenko.tm.service.ProjectService;
import ru.tsc.avramenko.tm.service.ProjectTaskService;
import ru.tsc.avramenko.tm.service.TaskService;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectTaskController projectTaskController =
            new ProjectTaskController(projectService, taskService, projectTaskService);


    public void start(String[] args) {
        displayWelcome();
        initData();
        parseArgs(args);
        process();
    }

    private void initData() {
        projectService.add(new Project("Project C", "1"));
        projectService.add(new Project("Project A", "2"));
        projectService.add(new Project("Project B", "3"));
        projectService.add(new Project("Project D", "4"));
        taskService.add(new Task("Task C", "1"));
        taskService.add(new Task("Task A", "2"));
        taskService.add(new Task("Task B", "3"));
        taskService.add(new Task("Task D", "4"));
        projectService.finishByName("Project B");
        projectService.startByName("Project A");
        taskService.finishByName("Task C");
        taskService.startByName("Task B");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void process() {
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                parseCommand(command);
            } catch (final Exception e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            default:
                throw new UnknownCommandException();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeStatusByName();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID:
                projectTaskController.findAllTasksByProjectId();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectTaskController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectTaskController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectTaskController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeStatusByName();
                break;
            default:
                throw new UnknownCommandException();
        }
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}